

import java.util.ArrayList;
import java.util.List;

public class PCB {
    private String pName;//进程名称
    private List pInstructions = new ArrayList<Instructions>();//进程中的指令列表
    private int CurrentInstruction;        //当前运行指令索引

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public int getCurrentInstruction() {
        return CurrentInstruction;
    }

    public void setCurrentInstruction(int currentInstruction) {
        CurrentInstruction = currentInstruction;
    }

    public List<Instructions> getpInstructions() {
        return pInstructions;
    }

    public void setpInstructions(List<Instructions> pInstructions) {
        this.pInstructions = pInstructions;
    }
}
